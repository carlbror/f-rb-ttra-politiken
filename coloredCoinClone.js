if(Meteor.isClient){


    $(function(){
        $('a[href*=#]:not([href=#])').click(function(){
            if(location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname){
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if(target.length){
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });


    $(document).ready(function(){

        $(".owl-carousel").owlCarousel({

            navigation: false, // Show next and prev buttons
            pagination: false,
            mouseDrag : false,
            touchDrag : false,

            autoPlay : 15000,
            stopOnHover : false,
            paginationSpeed : 2000,
            singleItem : true,
            transitionStyle:"fade"

            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false

        });

    });


}